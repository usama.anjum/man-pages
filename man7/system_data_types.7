.\" Copyright (c) 2020 by Alejandro Colomar <colomar.6.4.3@gmail.com>
.\" and Copyright (c) 2020 by Michael Kerrisk <mtk.manpages@gmail.com>
.\"
.\" SPDX-License-Identifier: Linux-man-pages-copyleft
.\"
.\"
.TH SYSTEM_DATA_TYPES 7 2021-03-22 "Linux" "Linux Programmer's Manual"
.SH NAME
system_data_types \- overview of system data types
.SH DESCRIPTION
.\" Layout:
.\"	A list of type names (the struct/union keyword will be omitted).
.\"	Each entry will have the following parts:
.\"		* Include (see NOTES)
.\"
.\"		* Definition (no "Definition" header)
.\"			Only struct/union types will have definition;
.\"			typedefs will remain opaque.
.\"
.\"		* Description (no "Description" header)
.\"			A few lines describing the type.
.\"
.\"		* Versions (optional)
.\"
.\"		* Conforming to (see NOTES)
.\"			Format: CXY and later; POSIX.1-XXXX and later.
.\"
.\"		* Notes (optional)
.\"
.\"		* Bugs (if any)
.\"
.\"		* See also
.\"------------------------------------- aiocb ------------------------/
.\"------------------------------------- blkcnt_t ---------------------/
.\"------------------------------------- blksize_t --------------------/
.\"------------------------------------- cc_t -------------------------/
.\"------------------------------------- clock_t ----------------------/
.\"------------------------------------- clockid_t --------------------/
.\"------------------------------------- dev_t ------------------------/
.\"------------------------------------- div_t ------------------------/
.\"------------------------------------- double_t ---------------------/
.\"------------------------------------- fd_set -----------------------/
.\"------------------------------------- fenv_t -----------------------/
.\"------------------------------------- fexcept_t --------------------/
.\"------------------------------------- FILE -------------------------/
.\"------------------------------------- float_t ----------------------/
.\"------------------------------------- gid_t ------------------------/
.\"------------------------------------- id_t -------------------------/
.\"------------------------------------- imaxdiv_t --------------------/
.\"------------------------------------- intmax_t ---------------------/
.\"------------------------------------- intN_t -----------------------/
.\"------------------------------------- intptr_t ---------------------/
.\"------------------------------------- lconv ------------------------/
.\"------------------------------------- ldiv_t -----------------------/
.\"------------------------------------- lldiv_t ----------------------/
.\"------------------------------------- mode_t -----------------------/
.\"------------------------------------- off64_t ----------------------/
.TP
.I off64_t
.RS
.IR Include :
.IR <sys/types.h> .
.PP
Used for file sizes.
It is a 64-bit signed integer type.
.PP
.IR "Conforming to" :
Present in glibc.
It is not standardized by the C language standard nor POSIX.
.PP
.IR Notes :
The feature test macro
.B _LARGEFILE64_SOURCE
has to be defined for this type to be available.
.PP
.IR "See also" :
.BR copy_file_range (2),
.BR readahead (2),
.BR sync_file_range (2),
.BR lseek64 (3),
.BR feature_test_macros (7)
.PP
See also the
.\" .I loff_t
.\" and
.I off_t
type in this page.
.RE
.\"------------------------------------- off_t ------------------------/
.TP
.I off_t
.RS
.IR Include :
.IR <sys/types.h> .
Alternatively,
.IR <aio.h> ,
.IR <fcntl.h> ,
.IR <stdio.h> ,
.IR <sys/mman.h> ,
.IR <sys/stat.h.h> ,
or
.IR <unistd.h> .
.PP
Used for file sizes.
According to POSIX,
this shall be a signed integer type.
.PP
.IR Versions :
.I <aio.h>
and
.I <stdio.h>
define
.I off_t
since POSIX.1-2008.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR Notes :
On some architectures,
the width of this type can be controlled with the feature test macro
.BR _FILE_OFFSET_BITS .
.PP
.IR "See also" :
.\" .BR fallocate (2),
.BR lseek (2),
.BR mmap (2),
.\" .BR mmap2 (2),
.BR posix_fadvise (2),
.BR pread (2),
.\" .BR preadv (2),
.BR truncate (2),
.BR fseeko (3),
.\" .BR getdirentries (3),
.BR lockf (3),
.BR posix_fallocate (3),
.BR feature_test_macros (7)
.PP
See also the
.\" .I loff_t
.\" and
.I off64_t
type in this page.
.RE
.\"------------------------------------- pid_t ------------------------/
.\"------------------------------------- ptrdiff_t --------------------/
.TP
.I ptrdiff_t
.RS
.IR Include :
.IR <stddef.h> .
.PP
Used for a count of elements, and array indices.
It is the result of subtracting two pointers.
According to the C language standard, it shall be a signed integer type
capable of storing values in the range
.RB [ PTRDIFF_MIN ,
.BR PTRDIFF_MAX ].
.PP
The length modifier for
.I ptrdiff_t
for the
.BR printf (3)
and the
.BR scanf (3)
families of functions is
.BR t ;
resulting commonly in
.B %td
or
.B %ti
for printing
.I ptrdiff_t
values.
.PP
.IR "Conforming to" :
C99 and later; POSIX.1-2001 and later.
.PP
.IR "See also" :
the
.I size_t
and
.I ssize_t
types in this page.
.RE
.\"------------------------------------- regex_t ----------------------/
.TP
.I regex_t
.RS
.IR Include :
.IR <regex.h> .
.PP
.EX
typedef struct {
    size_t  re_nsub; /* Number of parenthesized subexpressions */
} regex_t;
.EE
.PP
This is a structure type used in regular expression matching.
It holds a compiled regular expression, compiled with
.BR regcomp (3).
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR regex (3)
.RE
.\"------------------------------------- regmatch_t -------------------/
.TP
.I regmatch_t
.RS
.IR Include :
.IR <regex.h> .
.PP
.EX
typedef struct {
    regoff_t    rm_so; /* Byte offset from start of string
                          to start of substring */
    regoff_t    rm_eo; /* Byte offset from start of string of
                          the first character after the end of
                          substring */
} regmatch_t;
.EE
.PP
This is a structure type used in regular expression matching.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR regexec (3)
.RE
.\"------------------------------------- regoff_t ---------------------/
.TP
.I regoff_t
.RS
.IR Include :
.IR <regex.h> .
.PP
According to POSIX, it shall be a signed integer type
capable of storing the largest value that can be stored in either a
.I ptrdiff_t
type or a
.I ssize_t
type.
.PP
.IR Versions :
Prior to POSIX.1-2008, the type was capable of storing
the largest value that can be stored in either an
.I off_t
type or a
.I ssize_t
type.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
the
.I regmatch_t
structure and the
.I ptrdiff_t
and
.I ssize_t
types in this page.
.RE
.\"------------------------------------- sigevent ---------------------/
.TP
.I sigevent
.RS
.IR Include :
.IR <signal.h> .
Alternatively,
.IR <aio.h> ,
.IR <mqueue.h> ,
or
.IR <time.h> .
.PP
.EX
struct sigevent {
    int             sigev_notify; /* Notification type */
    int             sigev_signo;  /* Signal number */
    union sigval    sigev_value;  /* Signal value */
    void          (*sigev_notify_function)(union sigval);
                                  /* Notification function */
    pthread_attr_t *sigev_notify_attributes;
                                  /* Notification attributes */
};
.EE
.PP
For further details about this type, see
.BR sigevent (7).
.PP
.IR Versions :
.I <aio.h>
and
.I <time.h>
define
.I sigevent
since POSIX.1-2008.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR timer_create (2),
.BR getaddrinfo_a (3),
.BR lio_listio (3),
.BR mq_notify (3)
.PP
See also the
.I aiocb
structure in this page.
.RE
.\"------------------------------------- siginfo_t --------------------/
.TP
.I siginfo_t
.RS
.IR Include :
.IR <signal.h> .
Alternatively,
.IR <sys/wait.h> .
.PP
.EX
typedef struct {
    int      si_signo;  /* Signal number */
    int      si_code;   /* Signal code */
    pid_t    si_pid;    /* Sending process ID */
    uid_t    si_uid;    /* Real user ID of sending process */
    void    *si_addr;   /* Address of faulting instruction */
    int      si_status; /* Exit value or signal */
    union sigval si_value;  /* Signal value */
} siginfo_t;
.EE
.PP
Information associated with a signal.
For further details on this structure
(including additional, Linux-specific fields), see
.BR sigaction (2).
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR pidfd_send_signal (2),
.BR rt_sigqueueinfo (2),
.BR sigaction (2),
.BR sigwaitinfo (2),
.BR psiginfo (3)
.RE
.\"------------------------------------- sigset_t ---------------------/
.TP
.I sigset_t
.RS
.IR Include :
.IR <signal.h> .
Alternatively,
.IR <spawn.h> ,
or
.IR <sys/select.h> .
.PP
This is a type that represents a set of signals.
According to POSIX, this shall be an integer or structure type.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR epoll_pwait (2),
.BR ppoll (2),
.BR pselect (2),
.BR sigaction (2),
.BR signalfd (2),
.BR sigpending (2),
.BR sigprocmask (2),
.BR sigsuspend (2),
.BR sigwaitinfo (2),
.BR signal (7)
.RE
.\"------------------------------------- sigval -----------------------/
.TP
.I sigval
.RS
.IR Include :
.IR <signal.h> .
.PP
.EX
union sigval {
    int     sigval_int; /* Integer value */
    void   *sigval_ptr; /* Pointer value */
};
.EE
.PP
Data passed with a signal.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR pthread_sigqueue (3),
.BR sigqueue (3),
.BR sigevent (7)
.PP
See also the
.I sigevent
structure
and the
.I siginfo_t
type
in this page.
.RE
.\"------------------------------------- size_t -----------------------/
.TP
.I size_t
.RS
.IR Include :
.I <stddef.h>
or
.IR <sys/types.h> .
Alternatively,
.IR <aio.h> ,
.IR <glob.h> ,
.IR <grp.h> ,
.IR <iconv.h> ,
.IR <monetary.h> ,
.IR <mqueue.h> ,
.IR <ndbm.h> ,
.IR <pwd.h> ,
.IR <regex.h> ,
.IR <search.h> ,
.IR <signal.h> ,
.IR <stdio.h> ,
.IR <stdlib.h> ,
.IR <string.h> ,
.IR <strings.h> ,
.IR <sys/mman.h> ,
.IR <sys/msg.h> ,
.IR <sys/sem.h> ,
.IR <sys/shm.h> ,
.IR <sys/socket.h> ,
.IR <sys/uio.h> ,
.IR <time.h> ,
.IR <unistd.h> ,
.IR <wchar.h> ,
or
.IR <wordexp.h> .
.PP
Used for a count of bytes.
It is the result of the
.I sizeof
operator.
According to the C language standard,
it shall be an unsigned integer type
capable of storing values in the range [0,
.BR SIZE_MAX ].
According to POSIX,
the implementation shall support one or more programming environments
where the width of
.I size_t
is no greater than the width of the type
.IR long .
.PP
The length modifier for
.I size_t
for the
.BR printf (3)
and the
.BR scanf (3)
families of functions is
.BR z ;
resulting commonly in
.B %zu
or
.B %zx
for printing
.I size_t
values.
.PP
.IR Versions :
.IR <aio.h> ,
.IR <glob.h> ,
.IR <grp.h> ,
.IR <iconv.h> ,
.IR <mqueue.h> ,
.IR <pwd.h> ,
.IR <signal.h> ,
and
.I <sys/socket.h>
define
.I size_t
since POSIX.1-2008.
.PP
.IR "Conforming to" :
C99 and later; POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR read (2),
.BR write (2),
.BR fread (3),
.BR fwrite (3),
.BR memcmp (3),
.BR memcpy (3),
.BR memset (3),
.BR offsetof (3)
.PP
See also the
.I ptrdiff_t
and
.I ssize_t
types in this page.
.RE
.\"------------------------------------- sockaddr ---------------------/
.\"------------------------------------- socklen_t --------------------/
.\"------------------------------------- ssize_t ----------------------/
.TP
.I ssize_t
.RS
.IR Include :
.IR <sys/types.h> .
Alternatively,
.IR <aio.h> ,
.IR <monetary.h> ,
.IR <mqueue.h> ,
.IR <stdio.h> ,
.IR <sys/msg.h> ,
.IR <sys/socket.h> ,
.IR <sys/uio.h> ,
or
.IR <unistd.h> .
.PP
Used for a count of bytes or an error indication.
According to POSIX, it shall be a signed integer type
capable of storing values at least in the range [-1,
.BR SSIZE_MAX ],
and the implementation shall support one or more programming environments
where the width of
.I ssize_t
is no greater than the width of the type
.IR long .
.PP
Glibc and most other implementations provide a length modifier for
.I ssize_t
for the
.BR printf (3)
and the
.BR scanf (3)
families of functions, which is
.BR z ;
resulting commonly in
.B %zd
or
.B %zi
for printing
.I ssize_t
values.
Although
.B z
works for
.I ssize_t
on most implementations,
portable POSIX programs should avoid using it\(emfor example,
by converting the value to
.I intmax_t
and using its length modifier
.RB ( j ).
.PP
.IR Versions :
.IR <aio.h> ,
.IR <mqueue.h> ,
and
.I <sys/socket.h>
define
.I size_t
since POSIX.1-2008.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR read (2),
.BR readlink (2),
.BR readv (2),
.BR recv (2),
.BR send (2),
.BR write (2)
.PP
See also the
.I ptrdiff_t
and
.I size_t
types in this page.
.RE
.\"------------------------------------- stat -------------------------/
.TP
.I stat
.RS
.IR Include :
.IR <sys/stat.h> .
Alternatively,
.IR <ftw.h> .
.PP
.EX
struct stat {
    dev_t     st_dev;     /* ID of device containing file */
    ino_t     st_ino;     /* Inode number */
    mode_t    st_mode:    /* File type and mode */
    nlink_t   st_nlink;   /* Number of hard links */
    uid_t     st_uid;     /* User ID of owner */
    gid_t     st_gid;     /* Group ID of owner */
    dev_t     st_rdev;    /* Device ID (if special file) */
    off_t     st_size;    /* Total size, in bytes */
    blksize_t st_blksize; /* Block size for filesystem I/O */
    blkcnt_t  st_blocks;  /* Number of 512 B blocks allocated */

    /* Since POSIX.1-2008, this structure supports nanosecond
       precision for the following timestamp fields.
       For the details before POSIX.1-2008, see Notes. */

    struct timespec st_atim;  /* Time of last access */
    struct timespec st_mtim;  /* Time of last modification */
    struct timespec st_ctim;  /* Time of last status change */

#define st_atime st_atim.tv_sec  /* Backward compatibility */
#define st_mtine st_mtim.tv_sec
#define st_ctime st_ctim.tv_sec
};
.EE
.PP
Describes information about a file.
.PP
The fields are as follows:
.TP
.I st_dev
This field describes the device on which this file resides.
(The
.BR major (3)
and
.BR minor (3)
macros may be useful to decompose the device ID in this field.)
.TP
.I st_ino
This field contains the file's inode number.
.TP
.I st_mode
This field contains the file type and mode.
See
.BR inode (7)
for further information.
.TP
.I st_nlink
This field contains the number of hard links to the file.
.TP
.I st_uid
This field contains the user ID of the owner of the file.
.TP
.I st_gid
This field contains the ID of the group owner of the file.
.TP
.I st_rdev
This field describes the device that this file (inode) represents.
.TP
.I st_size
This field gives the size of the file (if it is a regular
file or a symbolic link) in bytes.
The size of a symbolic link is the length of the pathname
it contains, without a terminating null byte.
.TP
.I st_blksize
This field gives the "preferred" block size for efficient filesystem I/O.
.TP
.I st_blocks
This field indicates the number of blocks allocated to the file,
in 512-byte units.
(This may be smaller than
.IR st_size /512
when the file has holes.)
.TP
.I st_atime
This is the time of the last access of file data.
.TP
.I st_mtime
This is the time of last modification of file data.
.TP
.I st_ctime
This is the file's last status change timestamp
(time of last change to the inode).
.PP
For further information on the above fields, see
.BR inode (7).
.\"
.PP
.IR "Conforming to" :
POSIX.1-2001 and later (see Notes).
.PP
.IR Notes :
Old kernels and old standards did not support nanosecond timestamp fields.
Instead, there were three timestamp
.RI fields\(em st_atime ,
.IR st_mtime ,
and
.IR st_ctime \(emtyped
as
.I time_t
that recorded timestamps with one-second precision.
.PP
Since Linux 2.5.48, the
.I stat
structure supports nanosecond resolution for the three file timestamp fields.
The nanosecond components of each timestamp are available
via names of the form
.IR st_atim.tv_nsec ,
if suitable test macros are defined.
Nanosecond timestamps were standardized in POSIX.1-2008,
and, starting with version 2.12,
glibc exposes the nanosecond component names if
.B _POSIX_C_SOURCE
is defined with the value 200809L or greater, or
.B _XOPEN_SOURCE
is defined with the value 700 or greater.
Up to and including glibc 2.19,
the definitions of the nanoseconds components are also defined if
.B _BSD_SOURCE
or
.B _SVID_SOURCE
is defined.
If none of the aforementioned macros are defined,
then the nanosecond values are exposed with names of the form
.IR st_atimensec .
.PP
.IR "See also" :
.BR stat (2),
.BR inode (7)
.RE
.\"------------------------------------- suseconds_t ------------------/
.TP
.I suseconds_t
.RS
.IR Include :
.IR <sys/types.h> .
Alternatively,
.IR <sys/select.h> ,
or
.IR <sys/time.h> .
.PP
Used for time in microseconds.
According to POSIX, it shall be a signed integer type
capable of storing values at least in the range [-1, 1000000],
and the implementation shall support one or more programming environments
where the width of
.I suseconds_t
is no greater than the width of the type
.IR long .
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
the
.I timeval
structure in this page.
.RE
.\"------------------------------------- time_t -----------------------/
.TP
.I time_t
.RS
.IR Include :
.I <time.h>
or
.IR <sys/types.h> .
Alternatively,
.IR <sched.h> ,
.IR <sys/msg.h> ,
.IR <sys/select.h> ,
.IR <sys/sem.h> ,
.IR <sys/shm.h> ,
.IR <sys/stat.h> ,
.IR <sys/time.h> ,
or
.IR <utime.h> .
.PP
Used for time in seconds.
According to POSIX, it shall be an integer type.
.\" In POSIX.1-2001, the type was specified as being either an integer
.\" type or a real-floating type. However, existing implementations
.\" used an integer type, and POSIX.1-2008 tightened the specification
.\" to reflect this.
.PP
.IR Versions :
.I <sched.h>
defines
.I time_t
since POSIX.1-2008.
.PP
.IR "Conforming to" :
C99 and later; POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR stime (2),
.BR time (2),
.BR ctime (3),
.BR difftime (3)
.RE
.\"------------------------------------- timer_t ----------------------/
.TP
.I timer_t
.RS
.IR Include :
.IR <sys/types.h> .
Alternatively,
.IR <time.h> .
.PP
Used for timer ID returned by
.BR timer_create (2).
According to POSIX,
there are no defined comparison or assignment operators for this type.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR timer_create (2),
.BR timer_delete (2),
.BR timer_getoverrun (2),
.BR timer_settime (2)
.RE
.\"------------------------------------- timespec ---------------------/
.TP
.I timespec
.RS
.IR Include :
.IR <time.h> .
Alternatively,
.IR <aio.h> ,
.IR <mqueue.h> ,
.IR <sched.h> ,
.IR <signal.h> ,
.IR <sys/select.h> ,
or
.IR <sys/stat.h> .
.PP
.EX
struct timespec {
    time_t  tv_sec;  /* Seconds */
    long    tv_nsec; /* Nanoseconds [0 .. 999999999] */
};
.EE
.PP
Describes times in seconds and nanoseconds.
.PP
.IR "Conforming to" :
C11 and later; POSIX.1-2001 and later.
.PP
.IR Bugs :
Under glibc,
.I tv_nsec
is the
.I syscall
long, though this affects only fringe architectures like X32,
which is ILP32, but uses the LP64 AMD64 syscall ABI.
In reality, the field ends up being defined as:
.PP
.in +4
.EX
#if __x86_64__ && __ILP32__  /* == x32 */
    long long tv_nsec;
#else
    long      tv_nsec;
#endif
.EE
.in
.PP
This is a long-standing and long-enshrined
.UR https://sourceware.org/bugzilla/show_bug.cgi?id=16437
glibc bug
.I #16437
.UE ,
and an incompatible extension to the standards;
however, as even a 32-bit
.I long
can hold the entire
.I tv_nsec
range, it's always safe to forcibly down-cast it to the standard type.
.PP
.IR "See also" :
.BR clock_gettime (2),
.BR clock_nanosleep (2),
.BR nanosleep (2),
.BR timerfd_gettime (2),
.BR timer_gettime (2)
.RE
.\"------------------------------------- timeval ----------------------/
.TP
.I timeval
.RS
.IR Include :
.IR <sys/time.h> .
Alternatively,
.IR <sys/resource.h> ,
.IR <sys/select.h> ,
or
.IR <utmpx.h> .
.PP
.EX
struct timeval {
    time_t      tv_sec;  /* Seconds */
    suseconds_t tv_usec; /* Microseconds */
};
.EE
.PP
Describes times in seconds and microseconds.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR gettimeofday (2),
.BR select (2),
.BR utimes (2),
.BR adjtime (3),
.BR futimes (3),
.BR timeradd (3)
.RE
.\"------------------------------------- uid_t ----------------------/
.\"------------------------------------- uintmax_t --------------------/
.\"------------------------------------- uintN_t ----------------------/
.\"------------------------------------- uintptr_t --------------------/
.\"------------------------------------- useconds_t -------------------/
.TP
.I useconds_t
.RS
.IR Include :
.IR <sys/types.h> .
.PP
Used for time in microseconds.
According to POSIX, it shall be an unsigned integer type
capable of storing values at least in the range [0, 1000000],
and the implementation shall support one or more programming environments
where the width of
.I useconds_t
is no greater than the width of the type
.IR long .
.PP
.IR Versions :
POSIX.1-2001 defined
.I useconds_t
in
.I <unistd.h>
too.
.PP
.IR "Conforming to" :
POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR usleep (3)
.RE
.\"------------------------------------- va_list ----------------------/
.TP
.I va_list
.RS
.IR Include :
.IR <stdarg> .
Alternatively,
.IR <stdio.h> ,
or
.IR <wchar.h> .
.PP
Used by functions with a varying number of arguments of varying types.
The function must declare an object of type
.I va_list
which is used by the macros
.BR va_start (3),
.BR va_arg (3),
.BR va_copy (3),
and
.BR va_end (3)
to traverse the list of arguments.
.PP
.IR "Conforming to" :
C99 and later; POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR va_start (3),
.BR va_arg (3),
.BR va_copy (3),
.BR va_end (3)
.RE
.\"------------------------------------- void * -----------------------/
.TP
.I void *
.RS
According to the C language standard,
a pointer to any object type may be converted to a pointer to
.I void
and back.
POSIX further requires that any pointer,
including pointers to functions,
may be converted to a pointer to
.I void
and back.
.PP
Conversions from and to any other pointer type are done implicitly,
not requiring casts at all.
Note that this feature prevents any kind of type checking:
the programmer should be careful not to convert a
.I void *
value to a type incompatible to that of the underlying data,
because that would result in undefined behavior.
.PP
This type is useful in function parameters and return value
to allow passing values of any type.
The function will typically use some mechanism to know
the real type of the data being passed via a pointer to
.IR void .
.PP
A value of this type can't be dereferenced,
as it would give a value of type
.IR void ,
which is not possible.
Likewise, pointer arithmetic is not possible with this type.
However, in GNU C, pointer arithmetic is allowed
as an extension to the standard;
this is done by treating the size of a
.I void
or of a function as 1.
A consequence of this is that
.I sizeof
is also allowed on
.I void
and on function types, and returns 1.
.PP
The conversion specifier for
.I void *
for the
.BR printf (3)
and the
.BR scanf (3)
families of functions is
.BR p .
.PP
.IR Versions :
The POSIX requirement about compatibility between
.I void *
and function pointers was added in
POSIX.1-2008 Technical Corrigendum 1 (2013).
.PP
.IR "Conforming to" :
C99 and later; POSIX.1-2001 and later.
.PP
.IR "See also" :
.BR malloc (3),
.BR memcmp (3),
.BR memcpy (3),
.BR memset (3)
.PP
See also the
.I intptr_t
and
.I uintptr_t
types in this page.
.RE
.\"--------------------------------------------------------------------/
.SH NOTES
The structures described in this manual page shall contain,
at least, the members shown in their definition, in no particular order.
.PP
Most of the integer types described in this page don't have
a corresponding length modifier for the
.BR printf (3)
and the
.BR scanf (3)
families of functions.
To print a value of an integer type that doesn't have a length modifier,
it should be converted to
.I intmax_t
or
.I uintmax_t
by an explicit cast.
To scan into a variable of an integer type
that doesn't have a length modifier,
an intermediate temporary variable of type
.I intmax_t
or
.I uintmax_t
should be used.
When copying from the temporary variable to the destination variable,
the value could overflow.
If the type has upper and lower limits,
the user should check that the value is within those limits,
before actually copying the value.
The example below shows how these conversions should be done.
.SS Conventions used in this page
In "Conforming to" we only concern ourselves with
C99 and later and POSIX.1-2001 and later.
Some types may be specified in earlier versions of one of these standards,
but in the interests of simplicity we omit details from earlier standards.
.PP
In "Include", we first note the "primary" header(s) that
define the type according to either the C or POSIX.1 standards.
Under "Alternatively", we note additional headers that
the standards specify shall define the type.
.SH EXAMPLES
The program shown below scans from a string and prints a value stored in
a variable of an integer type that doesn't have a length modifier.
The appropriate conversions from and to
.IR intmax_t ,
and the appropriate range checks,
are used as explained in the notes section above.
.PP
.EX
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

int
main (void)
{
    static const char *const str = "500000 us in half a second";
    suseconds_t us;
    intmax_t    tmp;

    /* Scan the number from the string into the temporary variable. */

    sscanf(str, "%jd", &tmp);

    /* Check that the value is within the valid range of suseconds_t. */

    if (tmp < \-1 || tmp > 1000000) {
        fprintf(stderr, "Scanned value outside valid range!\en");
        exit(EXIT_FAILURE);
    }

    /* Copy the value to the suseconds_t variable \(aqus\(aq. */

    us = tmp;

    /* Even though suseconds_t can hold the value \-1, this isn\(aqt
       a sensible number of microseconds. */

    if (us < 0) {
        fprintf(stderr, "Scanned value shouldn\(aqt be negative!\en");
        exit(EXIT_FAILURE);
    }

    /* Print the value. */

    printf("There are %jd microseconds in half a second.\en",
            (intmax_t) us);

    exit(EXIT_SUCCESS);
}
.EE
.SH SEE ALSO
.BR feature_test_macros (7),
.BR standards (7)
